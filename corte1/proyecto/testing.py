#!/usr/bin/env python
import httplib2
import multiprocessing
from multiprocessing import Lock, Process, Queue, current_process

def console():
    print 'Iniciando el proceso: ', multiprocessing.current_process().name
    data = raw_input("Ingrese los datos desde la consola, escriba 'Quit' para salir ")
    while data.strip() != 'Quit':
    	work_queue.put(data)
    	#data = raw_input("Ingrese los datos desde la consola ")
	#work_queue.put(data)    	
	if data.strip() == 'Quit':
		while True:
			item = work_queue.get()
			print(item)
			if item is None:
				break
def web():
    print 'Iniciando el proceso: ', multiprocessing.current_process().name

def archive():
    print 'Iniciando el proceso: ', multiprocessing.current_process().name

work_queue = Queue()
done_queue = Queue()

available_actions = {"consola": console,
					"web":web,
					"archivo de texto": archive}

print "Projecto Primer Corte - Procesos\n De donde tomo los datos?\n    a. consola\n    b. web\n    c. archivo de texto"

action = raw_input()

try:
    available_actions[action]()
except KeyError:
    print "Error no existe la opcion: ", action
