import multiprocessing
import time

def Proceso_B(cond):
    name = multiprocessing.current_process().name
    print 'Iniciando Proceso: ', name
    with cond:
	time.sleep(15)
	print 'Datos recibidos por el proceso B: ', message
        cond.notify_all()

def Proceso_C(cond):
    name = multiprocessing.current_process().name
    print 'Iniciando Proceso: ', name
    with cond:
        cond.wait()
        print '%s Iniciado y Corriendo' % name

if __name__ == '__main__':

    message = input("Recepcion de Informacion: ")
    condition = multiprocessing.Condition()
    s1 = multiprocessing.Process(name='Captura de Datos', target=Proceso_B, args=(condition,))
    s1.start()

    s2_clients = [
	multiprocessing.Process(name='Proceso [%d]' % i, target=Proceso_C, args=(condition,))
        for i in range(1, 3)
        ]
    s2_clients.start()

