import os

forked_pid = os.fork()
if forked_pid == 0:
	print "[child] Hola Padre"
	print "[child] PID: %d" % (os.getpid(), )
else:
	print "[parent] Hola Mundo"
	print "[parent] PID: %d" % (os.getpid(), )

