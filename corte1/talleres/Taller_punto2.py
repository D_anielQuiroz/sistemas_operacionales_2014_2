#!/usr/bin/env python

import os, sys

print "El proceso padre crea y escribe datos en un archivo"

route = "Archivo.txt"

pid = os.fork()

if pid==0:
    #child
    r = open(route, 'r')
    print "child: Abriendo " +route
    txt = r.read()
    print txt
    r.close()
else:
    #child
    w = open(route, 'w')
    print "parent: Escribiendo"
    w.write("El padre escribio algo en el archivo")
    w.close()
    print "parent: closing"
    sys.exit(0)

